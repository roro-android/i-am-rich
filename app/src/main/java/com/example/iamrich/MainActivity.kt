package com.example.iamrich

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.FrameLayout
import com.bumptech.glide.Glide
import com.example.iamrich.databinding.ActivityMainBinding
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.initialization.InitializationStatus
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    lateinit var mAdView: FrameLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        Glide.with(this)
            .load(R.drawable.golden_diamond)
            .into(binding.ivImage)

        MobileAds.initialize(
            this
        ) { initializationStatus: InitializationStatus ->
            Log.d(
                "MyApplication",
                initializationStatus.toString()
            )
        }

        val adView = AdView(this)
        adView.adUnitId = "ca-app-pub-3940256099942544/6300978111"
        adView.adSize = AdSize.BANNER
        binding.containerAdsView.addView(adView)
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)

    }
}